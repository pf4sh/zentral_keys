# zentral_keys

Insert ssh-pubkeys, configs and derive the encrypted version, ready for deploy

Auth-Keys must contain a line with `#keys`.

The file-tree in keys looks like this:

```
keys
├── host1
│   ├── conf_user1
│   ├── conf_user1-R
│   ├── user1
│   └── user2
├── host2
│   └── user1
└── host3
    └── user2

```

Filesnames start with `conf_` will encrypted with pub-keys optional provided in
files ends with `-R`. Only the user(s) can decrypt such files.
