#!/bin/sh

# age-keygen -o /etc/ssh/key.txt
# chown nobody:wheel /etc/ssh/key.txt && chmod 460 /etc/ssh/key.txt
#
PUB="/tmp/pub_key"
if [ ! -f $PUB ]; then
    if [ -r "/etc/ssh/key.txt" ]; then
        grep public /etc/ssh/key.txt | awk -F': ' '{print $2}' > /tmp/pub_key
    else
        echo "no pub_key found"
        exit 2
    fi
fi
if [ ! -x "/usr/bin/age" ]; then
    echo "age not found"
    exit 2
fi

if [ ! -d keys ]; then
    echo "no keys to deploy"
    exit 1
fi
rm -rf deploy
cd keys || exit 1
for i in $(find ./ | sort); do
    [ "$i" = "." ] && continue
    [ -d "$i" ] && continue
    HR=$(dirname "$i" | sed s/'\.\/'/''/)
    UR=$(basename "$i")
    if echo "$i" | grep '\-R$' > /dev/null; then
        continue
    fi
    U=$(echo "$UR" | sha256sum | awk '{print $1}')
    H=$(echo "$HR" | sha256sum | awk '{print $1}')
    if [ ! -d "../deploy/$H" ]; then
        mkdir -p "../deploy/$H" || exit 3
    fi
    if grep '^#keys' "$HR/$UR" > /dev/null; then
        echo "authkeys for $HR/$UR -> $U"
        age -a -R "$PUB"< "$HR/$UR" > "../deploy/$H/$U"
    else
        if [ -r "$i-R" ]; then
            echo "user-config $HR/$UR-> $U"
            age -a -R "$i-R" < "$HR/$UR" > "../deploy/$H/$U"
        else
            echo "global-config $HR/$UR-> $U"
            age -a -R "$PUB" < "$HR/$UR" > "../deploy/$H/$U"
        fi
    fi
done
