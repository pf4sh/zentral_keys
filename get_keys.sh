#!/bin/sh

# remember the filepath and the file must owend by root
if [ ! -x "$(which age)" ]; then
    echo "age not found"
    exit 2
fi
if [ -x "/bin/sha256" ]; then
    SHA="/bin/sha256"
else
    SHA="command sha256sum"
fi
NETFS="/netfs/zentral"
PKEY="/etc/ssh/key.txt"
if [ $# -eq 2 ]; then
    H=$(echo "$2" | $SHA | awk '{print $1}')
else
    H=$(hostname | $SHA | awk '{print $1}')
fi
U=$(echo "$1" | $SHA | awk '{print $1}')

[ -r "$PKEY" ] || exit 2
if [ -d "$NETFS/$H" ] && [ -r "$NETFS/$H/$U" ]; then
    age -d -i "$PKEY" < "$NETFS/$H/$U"
else
    >&2 echo "get via http"
    # import URL config
    . /etc/ssh/url
    curl -sf "$URL/$H/$U" | age -d -i "$PKEY"
fi
