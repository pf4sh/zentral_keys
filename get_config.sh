#!/bin/sh

if [ ! -x "/usr/bin/age" ]; then
    echo "age not found"
    exit 2
fi
H=$(echo "conf_$(hostname)" | sha256sum | awk '{print $1}')
U=$(echo "$1" | sha256sum | awk '{print $1}')

NETFS="/netfs/zentral"
PKEY="$2"
[ -r "$PKEY" ] || exit 2
if [ -d "NETFS/$H" ] && [ -r "$NETFS/$H/$U" ]; then
    age -d -i "$PKEY" < "$NETFS/$H/$U"
else
    echo "get via http"
    # import URL config
    . /etc/ssh/url
    curl -sf "$URL/$H/$U" | age -d -i "$PKEY" || exit 1
fi
